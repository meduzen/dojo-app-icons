# Dojo app icons

Ces gars-là restent plus débiles que vous.

## [Forum](https://forum.nintendojo.fr) icons

![:vice:](https://framagit.org/meduzen/dojo-app-icons/raw/forum-vice/forum/android-chrome-192x192.png)

1. Add these lines in HTML `<head>`:

```html
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#9999ff">
<meta name="apple-mobile-web-app-title" content=":vice:">
<meta name="application-name" content=":vice:">
<meta name="msapplication-TileColor" content="#9999ff">
<meta name="theme-color" content="#9999ff">
```

2. Add every files of the `/forum` folder to your root folder (e.g. `forum.nintendojo.fr`).
It’s a lot of files, but it ensures better compatibility with legacy browsers.
